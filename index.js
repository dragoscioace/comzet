/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const isOnline = require('is-online');
const moment = require('moment');
const util = require('util');
var mysql = require('mysql');
var puppeteer = require('puppeteer');

const dbConfig = {
    host: 'localhost',
    user: 'root',
    password: 'ddd',
    database: 'comzet'
};

process.setMaxListeners(0);

function mysql_real_escape_string(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\" + char;
        }
    });
}

async function getResult(command) {

    let connection;

    try {
        connection = await mysql.createConnection(dbConfig);
        const query = await util.promisify(connection.query).bind(connection);
        const rows = await query(command);
        return rows;
    } catch (e) {
        console.log('function get("' + command + '") - ' + e.message);
        return false;
    } finally {
        if (connection && connection.end) {
            connection.end();
        }
    }
}

let scrape = async () => {

    const checker = await isOnline();

    if (checker) {
        try {
            const browser = await puppeteer.launch({
                "headless": true,
                "args": ["--fast-start", "--disable-extensions", "--no-sandbox", "--incognito", "--unlimited-storage", "--full-memory-crash-report"],
                "ignoreHTTPSErrors": true
            });

            const subcategories = await getResult('SELECT * FROM `subcategories`;');

            if (subcategories.length !== 0) {

                for (let subcategory_index = 0; subcategory_index < subcategories.length; subcategory_index++) {

                    const subcategory_url = subcategories[subcategory_index]['url'] + '/c';
                    const page = await browser.newPage();
                    await page.setDefaultNavigationTimeout(0);
                    const promise = page.waitForNavigation({timeout: 0, waitUntil: 'networkidle2'});
                    await page.goto(`${subcategory_url}`);
                    await promise;

                    const result = await page.evaluate(() => {
                        let total = document.getElementsByClassName('title-phrasing title-phrasing-sm')[0].innerText.split(' ')[0];

                        return {
                            total
                        };
                    });

                    await page.close();

                    let countPages = Math.floor((parseInt(result['total']) - 1) / 60 + 1);
                    for (let pages_index = 0; pages_index < countPages; pages_index++) {

                        const page_url = subcategories[subcategory_index]['url'] + '/p' + (pages_index + 1) + '/c';
                        const page = await browser.newPage();
                        await page.setDefaultNavigationTimeout(0);
                        const promise = page.waitForNavigation({timeout: 0, waitUntil: 'networkidle2'});
                        await page.goto(`${page_url}`);
                        await promise;

                        const products = await page.evaluate(() => {
                            let items_title = document.getElementsByClassName('product-title js-product-url');

                            let name = new Array();
                            let url = new Array();
                            let pnew = new Array();
                            let pold = new Array();
                            let data = new Array();
                            let used = new Array();
                            let product_id = new Array();

                            for (let i = 0; i < items_title.length; i++) {
                                const regex = /[+-]?\d+(\.\d+)?/g;
                                let href = items_title[i].href.substr(0, items_title[i].href.length - 1);
                                let price = document.getElementsByClassName('product-new-price')[i].innerText;
                                if (items_title[i].href.lastIndexOf('#used-products') !== -1) {
                                    used.push(1);
                                } else {
                                    used.push(0);
                                }
                                if (items_title[i].href.lastIndexOf('#') !== -1) {
                                    href = items_title[i].href.substr(0, items_title[i].href.lastIndexOf('#') - 1);
                                }
//                            if (document.getElementsByClassName('product-badge product-badge--resealed')[i] !== undefined) {
//                                used.push(1);
//                            } else {
//                                used.push(0);
//                            }
//                            used.push(0);
                                name.push(items_title[i].innerText);
                                url.push(items_title[i].href);
//                        pnew.push(price.match(regex).map(function (v) {
//                            return parseFloat(v);
//                        })[0]);
                                pnew.push(price);
                                pold.push(document.getElementsByClassName('product-old-price')[i].innerText);
                                data.push(Date.now());
                                product_id.push(href.substr(href.lastIndexOf('/') + 1, href.length));
                            }
                            return {
                                name, url, pnew, pold, data, product_id, used
                            };
                        });

                        for (let i = 0; i < products['product_id'].length; i++) {

                            const unique = await getResult("SELECT * FROM `products` WHERE `product_id`='" + products['product_id'][i] + "';");
                            if (unique.length === 0) {
                                const res = await getResult("INSERT INTO `products`(`id`, `subcategory_id`, `name`, `url`, `product_id`) VALUES (NULL, " + subcategories[subcategory_index]['id'] + ", '" + mysql_real_escape_string(products['name'][i]) + "', '" + mysql_real_escape_string(products['url'][i]) + "', '" + mysql_real_escape_string(products['product_id'][i]) + "')");

                                if (res !== false) {
                                    const product = await getResult("SELECT * FROM `products` WHERE `product_id`='" + products['product_id'][i] + "';");

                                    if (product.length !== 0) {

                                        await getResult("INSERT INTO `history`(`id`, `product_id`, `pnew`, `pold`, `data`, `used`) VALUES (NULL, " + product[0]['id'] + ",'" + products['pnew'][i] + "', '" + products['pold'][i] + "', NULL, " + products['used'][i] + ");");
                                    }
                                }
                            } else {

                                const history = await getResult("SELECT * FROM `history` WHERE `product_id`='" + unique[0]['id'] + "' AND `pnew`='" + products['pnew'][i] + "' AND `pold`='" + products['pold'][i] + "';");

                                if (history.length === 0) {

                                    await getResult("INSERT INTO `history`(`id`, `product_id`, `pnew`, `pold`, `data`, `used`) VALUES (NULL, " + unique[0]['id'] + ",'" + products['pnew'][i] + "', '" + products['pold'][i] + "', NULL, " + products['used'][i] + ");");
                                }
                            }
                        }

                        products.length = 0;
                        await page.close();
                    }
                }

                console.log('Finish scan - ' + moment(new Date()).format('YYYY-MM-DD HH:mm:ss'));
                await browser.close();
                scrape();

            } else {
                console.log('No subcategories available');
            }

            // Nothing to do, so we close the browser
            await browser.close();

        } catch (e) {
            console.log(e.message);
            scrape(); // Solution to free memory | Close all chrome browser if exists
        }
    } else {
        scrape();
    }

};

let main = async () => {
    console.log('START COMZET');
    scrape();
};

main();